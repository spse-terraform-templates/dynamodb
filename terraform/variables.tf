variable "table_name" {
  description = "Name of the DynamoDB table"
  type        = string
}

variable "billing_mode" {
  description = "Billing mode of the DynamoDB table"
  type        = string
}

variable "hash_key" {
  description = "Hash key (Partition key) of the DynamoDB table. Set to 'LockID' if the table will be used as a terraform state lock table"
  type        = string
}

variable "hash_key_type" {
  description = "The type of the DynamoDB table hash key (Partition key)"
  type        = string
  default     = "S"
}

variable "range_key" {
  description = "Range key of the DynamoDB table"
  type        = string
  default     = ""
}

variable "read_capacity" {
  description = "Read capacity of the DynamoDB table"
  type        = number
  default     = 10
}
variable "write_capacity" {
  description = "Write capacity of the DynamoDB table"
  type        = number
  default     = 10
}

variable "gsi_read_capacity" {
  description = "Global Secondary Index read capacity of the DynamoDB table"
  type        = number
  default     = 10
}

variable "gsi_write_capacity" {
  description = "Global Secondary Index write capacity of the DynamoDB table"
  type        = number
  default     = 10
}

variable "dynamodb_attributes" {
  type = list(object({
    name = string
    type = string
  }))
  default = [
    {
      name = "id"
      type = "S"
    }
  ]
}
