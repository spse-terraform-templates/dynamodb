# dynamodb

A reusable terraform module for configuring DynamoDB tables according to Labs DNR Standards.

Will setup the following:

- DynamoDB Table
  - Default Encryption
  - Default TTL
  - Point in Time Recovery
  - Table Name
  - Table Read/Write Capacity
  - Table Stream
  - Table Stream View Type
  - Table Stream Enabled

## Pulling the Module

Because the module is hosted within a [publicly scoped GitLab group](https://sfgitlab.opr.statefarm.org/REDLabs/labs-experience/terraform-modules), authentication is not required in order to pull it. Reference the module in your terraform as shown in the [Usage](#usage) section, run `terraform init`, and the module will be pulled in. If you want to always pull the latest version of the module you should set your source parameter to `git::https://sfgitlab.opr.statefarm.org/REDLabs/labs-experience/terraform-modules/dynamodb.git`, otherwise you can reference a specific tag by adding the `ref` query paraemter to the end of the url like so `git::https://sfgitlab.opr.statefarm.org/REDLabs/labs-experience/terraform-modules/dynamodb?ref=1.0.0`.

## Change Requests

Feel free to submit an [Issue](https://sfgitlab.opr.statefarm.org/REDLabs/labs-experience/terraform-modules/dynamodb/-/issues) for bug fixes or enhancement requests.

## Contributing/Support

Our hope is that this module would be owned and maintained by the engineering community in the Labs department. If you are willing/able to resolve bugs and/or improve the capabilities provided by this module feel free to submit a [merge request](https://sfgitlab.opr.statefarm.org/REDLabs/labs-experience/terraform-modules/dynamodb/-/merge_requests) with a description of your changes to the `main` branch to be reviewed by the owners of this repo. If you have changes that may be more specific to your product you could fork the project instead.

## Usage

```hcl
module "my_custom_dynamodb_table" {
  source = "git::https://sfgitlab.opr.statefarm.org/REDLabs/labs-experience/terraform-modules/dynamodb.git"
  # required
  table_name = "my-table-name"
  billing_mode = "PAY PER REQUEST"
  # optional
  read_capacity = 10
  write_capacity = 10
  gsi_read_capacity = 10
  gsi_write_capacity = 10
  hash_key = "LockID"
  hash_key_type = "S"
  range_key = "LockType"

```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="table_name"></a> [table\_name](#table\_name) | Name of the DynamoDB table | `string` | `N/A` | yes |
| <a name="billing_mode"></a> [billing\_mode](#billing\_mode) | Billing mode of the DynamoDB table| `string` | `N/A` | yes |
| <a name="hash_key"></a> [hash\_key](#hash\_key) | Hash key (Partition key) of the DynamoDB table | `string` | `N/A` | yes |
| <a name="hash_key_type"></a> [hash\_key\_type](#hash\_key\_type) | The type of the DynamoDB table hash key | `string` | `N/A` | yes |
| <a name="range_key"></a> [range\_key](#range\_key) | Range key of the DynamoDB table | `string` | `""` | no || <a name="read_capacity"></a> [read\_capacity](#read\_capacity) | Read capacity of the DynamoDB table| `number` | `10` | no |
| <a name="write_capacity"></a> [write\_capacity](#write\_capacity) | Write capacity of the DynamoDB table| `number` | `10` | no |
| <a name="gsi_read_capacity"></a> [gsi\_read\_capacity](#gsi\_read\_capacity) | Global Secondary Index read capacity of the DynamoDB table| `number` | `10` | no |
| <a name="gsi_write_capacity"></a> [gsi\_write\_capacity](#gsi\_write\_capacity) | Global Secondary Index write capacity of the DynamoDB table| `number` | `10` | no |

## Outputs

| Name | Description | Type |
|------|-------------|------|
| <a name="table_id"></a> [table\_id](#table\_id) | The ID of the DynamoDB table created by the module | `string` |
| <a name="table_name"></a> [table\_name](#table\_name) | The name of the DynamoDB table created by the module | `string` |
| <a name="table_arn"></a> [table\_arn](#table\_arn) | The ARN of the DynamoDB table created by the module | `string` |
| <a name="table_stream_arn"></a> [table\_stream\_arn](#table\_stream\_arn) | The stream ARN of the DynamoDB table created by the module | `string` |
